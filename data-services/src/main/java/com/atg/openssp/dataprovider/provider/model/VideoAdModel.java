package com.atg.openssp.dataprovider.provider.model;

import com.atg.openssp.common.cache.dto.VideoAd;
import com.atg.openssp.common.core.broker.dto.VideoAdDto;
import com.atg.openssp.common.core.system.LocalContext;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.properties.ProjectProperty;

import javax.xml.bind.PropertyException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 */
public class VideoAdModel {
    private static final Logger log = LoggerFactory.getLogger(VideoAdModel.class);
    private static VideoAdModel singleton;
    private final VideoAdDto videoAdDto = new VideoAdDto();
    private final GsonBuilder builder;

    private VideoAdModel() {
        builder = new GsonBuilder();
        initVideoAds();
    }

    private void initVideoAds() {
        synchronized (videoAdDto) {
            videoAdDto.setVideoAd(DataStore.getInstance().lookupVideoAds().getVideoAd());
        }
    }

    public void exportVideoAds(String exportName) {
        if (LocalContext.isAdservingChannelEnabled()) {
            synchronized (videoAdDto) {
                try {
                    String location;
                    try {
                        location = ProjectProperty.getPropertiesResourceLocation()+"/";
                    } catch (PropertyException e) {
                        log.warn("property file not found.");
                        location="";
                    }
                    Gson gson = builder.create();

                    PrintWriter pw = new PrintWriter(new FileWriter(location+exportName+".json"));
                    pw.println(gson.toJson(videoAdDto));
                    pw.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    public void importVideoAds(String importName) {
        if (LocalContext.isAdservingChannelEnabled()) {
            synchronized (videoAdDto) {
                try {
                    String location;
                    //以URL形式获取工程的资源文件 classpath 路径, 得到以file:/为开头的URL
                    //例如返回: file:/D:/workspace/myproject01/WEB-INF/classes/
                    URL classPath = Thread.currentThread().getContextClassLoader().getResource("");
                    String proFilePath = classPath.toString();
                    //移除开通的file:/六个字符
                    proFilePath = proFilePath.substring(6);
                    //如果为window系统下,则把路径中的路径分隔符替换为window系统的文件路径分隔符
                    location = proFilePath.replace("/", java.io.File.separator);
                    Gson gson = builder.create();
                    String content = new String(Files.readAllBytes(Paths.get(location + importName+".json")), StandardCharsets.UTF_8);
                    VideoAdDto newData = gson.fromJson(content, VideoAdDto.class);
                    DataStore.getInstance().clearVideoAds();
                    for (VideoAd s : newData.getVideoAd()) {
                        DataStore.getInstance().insert(s);
                    }
                    initVideoAds();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    public void loadVideoAds() {
        if (LocalContext.isVideoAdDataServiceEnabled()) {
            synchronized (videoAdDto) {
                try {
                    String location;
                    //以URL形式获取工程的资源文件 classpath 路径, 得到以file:/为开头的URL
                    //例如返回: file:/D:/workspace/myproject01/WEB-INF/classes/
                    URL classPath = Thread.currentThread().getContextClassLoader().getResource("");
                    String proFilePath = classPath.toString();
                    //移除开通的file:/六个字符
                    proFilePath = proFilePath.substring(6);
                    //如果为window系统下,则把路径中的路径分隔符替换为window系统的文件路径分隔符
                    location = proFilePath.replace("/", java.io.File.separator);
                    Gson gson = builder.create();
                    String content = new String(Files.readAllBytes(Paths.get(location + "video_ad_db.json")), StandardCharsets.UTF_8);
                    VideoAdDto newData = gson.fromJson(content, VideoAdDto.class);
                    DataStore.getInstance().clearVideoAds();
                    for (VideoAd s : newData.getVideoAd()) {
                        DataStore.getInstance().insert(s);
                    }
                    initVideoAds();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    public void resetWith(Iterable<VideoAd> videos) {
        synchronized (videoAdDto) {
            DataStore.getInstance().clearVideoAds();
            for (VideoAd s : videos) {
                DataStore.getInstance().insert(s);
            }
            initVideoAds();
        }
    }

    public VideoAdDto lookupDto() {
        synchronized (videoAdDto) {
            VideoAdDto dto = new VideoAdDto();
            dto.setVideoAd(videoAdDto.getVideoAd());
            return dto;
        }
    }

    public void insert(VideoAd s) {
        synchronized (videoAdDto) {
            DataStore.getInstance().insert(s);
            initVideoAds();
        }
    }

    public void remove(VideoAd s) {
        synchronized (videoAdDto) {
            DataStore.getInstance().remove(s);
            initVideoAds();
        }
    }

    public void update(VideoAd s) {
        synchronized (videoAdDto) {
            DataStore.getInstance().update(s);
            initVideoAds();
        }
    }

    public void load() {
        synchronized (videoAdDto) {
            DataStore.getInstance().clearVideoAds();
            initVideoAds();
        }
    }

    public void clear() {
        synchronized (videoAdDto) {
            DataStore.getInstance().clearVideoAds();
            initVideoAds();
        }
    }

    public synchronized static VideoAdModel getInstance() {
        if (singleton == null) {
            singleton = new VideoAdModel();
        }
        return singleton;
    }

}
