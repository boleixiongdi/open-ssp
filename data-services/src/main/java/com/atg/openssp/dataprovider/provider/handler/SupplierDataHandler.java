package com.atg.openssp.dataprovider.provider.handler;

import com.atg.openssp.common.core.broker.dto.SupplierDto;
import com.atg.openssp.common.core.system.LocalContext;
import com.atg.openssp.common.core.system.loader.GlobalContextLoader;
import com.atg.openssp.common.demand.Supplier;
import com.atg.openssp.common.provider.DataHandler;
import com.atg.openssp.common.provider.LoginHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.properties.ProjectProperty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.PropertyException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

/**
 * @author Brian Sorensen
 */
public class SupplierDataHandler extends DataHandler {
    private static final Logger log = LoggerFactory.getLogger(SupplierDataHandler.class);
    public static final String CONTEXT = "/lookup/supplier";

    public SupplierDataHandler(HttpServletRequest request, HttpServletResponse response) {
        if (LocalContext.isSupplierDataServiceEnabled()) {
            try {
                String location;
                    //以URL形式获取工程的资源文件 classpath 路径, 得到以file:/为开头的URL
                    //例如返回: file:/D:/workspace/myproject01/WEB-INF/classes/
                    URL classPath = Thread.currentThread().getContextClassLoader().getResource("");
                    String proFilePath = classPath.toString();
                    //移除开通的file:/六个字符
                    proFilePath = proFilePath.substring(6);
                    //如果为window系统下,则把路径中的路径分隔符替换为window系统的文件路径分隔符
                    location = proFilePath.replace("/", java.io.File.separator);

                    GsonBuilder builder = new GsonBuilder();
                    Supplier.populateTypeAdapters(builder);
                    Gson gson = builder.create();
                    String content = new String(Files.readAllBytes(Paths.get(location+"supplier_db.json")), StandardCharsets.UTF_8);
                Map<String,String> parms = queryToMap(request.getQueryString());
                String t = parms.get("t");

                if (LoginHandler.TOKEN.equals(t)) {
//                    GsonBuilder builder = new GsonBuilder();
                    Supplier.populateTypeAdapters(builder);
                    String result = content; //builder.create().toJson(data);
                    response.setStatus(200);
                    response.setContentType("application/json; charset=UTF8");
                    OutputStream os = response.getOutputStream();
                    os.write(result.getBytes());
                    os.flush();
                    os.close();
                    log.info("<--"+result.replaceAll("\n", ""));
                } else {
                    response.setStatus(401);
                }
            } catch (Exception e) {
                response.setStatus(500);
                log.error(e.getMessage(), e);
            }
        } else {
            response.setStatus(404);
        }
    }

    @Override
    public void cleanUp() {

    }

}
